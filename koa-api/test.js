var app = require("./app.js");
var request = require("supertest").agent(app.listen());
var co = require("co");
var users = require("./user-routes.js").users;

describe("Simple user http crude API", function(){
  var a_user = {};

  beforeEach(function(done){
    a_user = {name: "Nicholas", age: 39, height: 6.2};
    removeAll(done)
  });

  afterEach(function(done){
    removeAll(done);
  });

  var removeAll = function(done) {
    co(function *(){
      yield users.remove({});
    }).then(done);
  };

  it("Delete User", function(done){
    co(function *(){
      var insertedUser = yield users.insert(a_user);
      var url = "/ghostmonkUser/" + insertedUser._id;
      request
        .del(url)
        .expect(200, done);
    })
  });

  it("Update User", function(done){
    co(function *() {
      var insertedUser = yield users.insert(a_user);
      var url = "/ghostmonkUser/" + insertedUser._id;

      request
        .put(url)
        .send({name: "NEW Nicholas", age: 39, height: 6.2})
        .expect("location", url)
        .expect(204, done);

    });
  });

  it("Post User", function(done){
    request
      .post("/ghostmonkUser")
      .send(a_user)
      .expect("location", /^\/ghostmonkUser\/[0-9a-fA-F]{24}$/)
      .expect(200, done)
  });

  it("Request without name", function(done){
    delete a_user.name;

    request
      .post("/ghostmonkUser")
      .send(a_user)
      .expect("name required")
      .expect(400, done);
  });

  it("Get User", function(done){

    co(function *() {
      var insertedUser = yield users.insert(a_user);
      var url = "/ghostmonkUser/" + insertedUser._id;

      request
        .get(url)
        .set("Accept", "application/json")
        .expect("Content-type", /json/)
        .expect(/Nicholas/)
        .expect(/6.2/)
        .expect(200, done);
    });

  });

});