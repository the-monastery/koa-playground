var koa = require("koa");
var app = module.exports = koa();

var routes = require("koa-route");

var userRoutes = require("./user-routes.js");
app.use(routes.post("/ghostmonkUser", userRoutes.addUser));
app.use(routes.get("/ghostmonkUser/:id", userRoutes.getUser));
app.use(routes.put("/ghostmonkUser/:id", userRoutes.updateUser));
app.use(routes.del("/ghostmonkUser/:id", userRoutes.deleteUser));

app.listen(3000);
console.log("The app is listening on port 3000.");

