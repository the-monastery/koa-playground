function *evenIntegers() {
  var i = 0;
  while(true) {
    yield i;
    i += 2;
  }
}

var even = evenIntegers();

console.log(even.next());
console.log(even.next());
console.log(even.next());
console.log(even.next());
console.log(even.next());
console.log(even.next());

//Next always returns the next object in the sequence
function *objectYield() {
  yield 21;
  yield {name:'Nicholas', age:39, kids:['Lucas', 'Aila', 'Logan']};
  yield "A string with data in it."
}

var newYield = objectYield();

console.log(newYield.next());
console.log(newYield.next());
console.log(newYield.next());

//This will return done == true
console.log(newYield.next());

////////////////////////////////////////////////////
var co = require("co");
var wait = require("co-wait");

co(function *(){

  console.log("Started Sequence");
  console.time("sequence");

  yield wait(1000);
  yield wait(2000);
  yield wait(3000);

  console.timeEnd("sequence");
  console.log("Completed Sequence");

}).then();

co(function *(){

  console.time("Parallel Started");

  var a = wait(1000);
  var b = wait(2000);
  var c = wait(3000);
  var res = yield [a, b, c];

  console.timeEnd("sequence");
  console.log("Parallel Completed");

}).then();


